package coding.challenge;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;

@SpringBootApplication
@OpenAPIDefinition(info = @Info(version = "1.0.0", description = "Coding-Challenge", title = "Coding-Challenge-Service"))
public class CodingChallengeBackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(CodingChallengeBackendApplication.class, args);
    }

}
