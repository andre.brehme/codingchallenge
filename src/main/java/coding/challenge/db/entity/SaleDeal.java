package coding.challenge.db.entity;

import jdk.jfr.Percentage;

public enum SaleDeal {

    None,
    Buy1Get1Free,
    TenPercentage,

}
