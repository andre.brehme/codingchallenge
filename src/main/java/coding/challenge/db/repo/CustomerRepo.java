package coding.challenge.db.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import coding.challenge.db.entity.Customer;

public interface CustomerRepo extends JpaRepository<Customer, Long> {}
