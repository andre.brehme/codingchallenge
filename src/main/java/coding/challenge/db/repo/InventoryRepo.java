package coding.challenge.db.repo;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import coding.challenge.db.entity.InventoryItem;
import coding.challenge.db.entity.Product;

public interface InventoryRepo extends JpaRepository<InventoryItem, String> {

    Optional<InventoryItem> findByProduct(Product product);
}
