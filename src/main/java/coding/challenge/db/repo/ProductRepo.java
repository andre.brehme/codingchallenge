package coding.challenge.db.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import coding.challenge.db.entity.Product;

public interface ProductRepo extends JpaRepository<Product, String> {}
