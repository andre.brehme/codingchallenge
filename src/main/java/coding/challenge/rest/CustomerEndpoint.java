package coding.challenge.rest;

import org.springframework.boot.context.properties.bind.DefaultValue;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import coding.challenge.service.CustomerService;

@RestController
@RequestMapping("api")
public class CustomerEndpoint implements CustomerEndpointI {

    private final CustomerService customerService;

    public CustomerEndpoint(CustomerService customerService) {
        this.customerService = customerService;
    }

    @PutMapping(path = "/kunde/{kundenId}/basket", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity addToBasket(@PathVariable long kundenId, @RequestParam String productId){
        customerService.addProductToBasket(kundenId, productId);
        return ResponseEntity.ok().build();
    }

    @GetMapping(path = "/kunde/{kundenId}/basket", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> getTotalBasketPrice(@PathVariable long kundenId, @RequestParam @DefaultValue("NONE") String saleDeal){
        double sum = customerService.getTotalBasketPrice(kundenId, saleDeal);
        return ResponseEntity.ok(String.valueOf(sum));
    }
}
