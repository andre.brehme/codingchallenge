package coding.challenge.rest;

import org.springframework.boot.context.properties.bind.DefaultValue;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

public interface CustomerEndpointI {

    @GetMapping(path = "/kunde/{kundenId}/basket/add", produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity addToBasket(@PathVariable long kundenId, @RequestParam String productIds);

    @GetMapping(path = "/kunde/{kundenId}/basket", produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<String> getTotalBasketPrice(@PathVariable long kundenId,
            @RequestParam @DefaultValue("NONE") String saleDeal);

}
