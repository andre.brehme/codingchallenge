package coding.challenge.service;

import java.math.BigDecimal;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import coding.challenge.db.entity.Customer;
import coding.challenge.db.entity.InventoryItem;
import coding.challenge.db.entity.Product;
import coding.challenge.db.entity.SaleDeal;
import coding.challenge.db.repo.CustomerRepo;
import coding.challenge.db.repo.InventoryRepo;
import coding.challenge.db.repo.ProductRepo;

@Component
public class CustomerService {

    private final CustomerRepo customerRepo;
    private final ProductRepo productRepo;
    private final InventoryRepo inventoryRepo;

    public CustomerService(CustomerRepo customerRepo, ProductRepo productRepo,
            InventoryRepo inventoryRepo) {
        this.customerRepo = customerRepo;
        this.productRepo = productRepo;
        this.inventoryRepo = inventoryRepo;
    }

    public void addProductToBasket(Long kundenId, String productId) {

        Customer customer = findCustomer(kundenId);
        Product product = findProduct(productId);
        if(producIsAvailable(product)){
            customer.addProduct(product);
            customerRepo.save(customer);
        }

    }

    public double getTotalBasketPrice(long kundenId, String saleDeal) {
        Optional<Customer> customer = customerRepo.findById(kundenId);
        if (customer.isPresent()) {
            List<Product> products = customer.get()
                    .getProducts();

            SaleDeal deal = SaleDeal.valueOf(saleDeal);
            return switch (deal) {
                case None -> getSum(products);
                case Buy1Get1Free -> getBuy1Get1Free(products);
                case TenPercentage -> getTenPercentageDiscount(products);
            };
        }
        throw new IllegalArgumentException("Customer not found!");
    }

    protected double getSum(List<Product> products) {
        return products
                .stream()
                .collect(Collectors.summarizingDouble(Product::getPrice))
                .getSum();
    }

    protected double getBuy1Get1Free(List<Product> products) {
        int freeProducts = products.size() == 1 ? 1 : products.size() / 2;
        List<Product> productsToPayFor = products.stream()
                .sorted(Comparator.comparing(Product::getPrice).reversed())
                .toList()
                .subList(0, freeProducts);

        return getSum(productsToPayFor);
    }

    protected double getTenPercentageDiscount(List<Product> products) {
        BigDecimal sum = BigDecimal.valueOf(getSum(products));
        BigDecimal discountSum = sum.multiply(new BigDecimal("0.9"));
        return discountSum.doubleValue();

    }

    protected Customer findCustomer(Long customerId) {
        return customerRepo.findById(customerId)
                .orElseThrow(() -> new IllegalArgumentException("Customer not found!"));
    }

    protected Product findProduct(String productId) {
        return productRepo.findById(productId)
                .orElseThrow(() -> new IllegalArgumentException("Product not found!"));
    }

    protected boolean producIsAvailable(Product product){
        InventoryItem inventoryEntry = inventoryRepo.findByProduct(product).orElseThrow(() -> new IllegalArgumentException("InventoryItem not found!"));
        return inventoryEntry.getAmount() > 0;
    }
}
