package coding.challenge.rest;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.ResponseEntity;
import org.springframework.web.util.UriComponentsBuilder;

import coding.challenge.db.entity.Customer;
import coding.challenge.db.entity.InventoryItem;
import coding.challenge.db.entity.Product;
import coding.challenge.db.repo.CustomerRepo;
import coding.challenge.db.repo.InventoryRepo;
import coding.challenge.db.repo.ProductRepo;
import support.PostgresTestcontainerExtension;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ExtendWith(PostgresTestcontainerExtension.class)
class CustomerEndpointIT {

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private ProductRepo productRepo;
    @Autowired
    private InventoryRepo inventoryRepo;
    @Autowired
    private CustomerRepo customerRepo;

    @BeforeEach
    void init() {
        Product product = new Product();
        product.setPrice(1);
        product.setId("A002");
        productRepo.save(product);

        InventoryItem inventoryItem = new InventoryItem();
        inventoryItem.setAmount(1);
        inventoryItem.setProduct(product);
        inventoryRepo.save(inventoryItem);

        Customer customer = new Customer();
        customerRepo.save(customer);

        UriComponentsBuilder builder = UriComponentsBuilder
                .fromUriString("http://localhost:" + port + "/api/kunde/1/basket")
                .queryParam("productId", "A002");
        restTemplate.put(builder.buildAndExpand(builder).toUri(), String.class);
    }

    @Test
    void getDiscountBasket() {

        UriComponentsBuilder builder = UriComponentsBuilder
                .fromUriString("http://localhost:" + port + "/api/kunde/1/basket")
                .queryParam("saleDeal", "Buy1Get1Free");

        ResponseEntity<String> forEntity = restTemplate.getForEntity(builder.buildAndExpand(builder).toUri(),
                String.class);

        assertThat(forEntity.getBody()).isEqualTo("1.0");
    }

}