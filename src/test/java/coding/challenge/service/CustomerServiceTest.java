package coding.challenge.service;

import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import coding.challenge.db.entity.Product;
import coding.challenge.db.repo.CustomerRepo;
import coding.challenge.db.repo.InventoryRepo;
import coding.challenge.db.repo.ProductRepo;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
class CustomerServiceTest {

    @Mock
    private CustomerRepo customerRepo;
    @Mock
    private ProductRepo productRepo;
    @Mock
    private InventoryRepo inventoryRepo;

    private CustomerService customerService;

    private final Product p1 = new Product();
    private final Product p2 = new Product();

    @BeforeEach
    void setUp() {
        customerService = new CustomerService(customerRepo, productRepo, inventoryRepo);
        p1.setPrice(1.1);
        p2.setPrice(1.2);
    }

    @Test
    void shouldSumUp() {
        double sum = customerService.getSum(List.of(p1, p2));
        assertThat(sum).isEqualTo(2.3);
    }

    @Test
    void shouldGet1Free() {
        double sum = customerService.getBuy1Get1Free(List.of(p1, p2));
        assertThat(sum).isEqualTo(1.2);
    }

    @Test
    void shouldGet10PercentageDiscount() {
        double sum = customerService.getTenPercentageDiscount(List.of(p1, p2));
        assertThat(sum).isEqualTo(2.07);
    }


}