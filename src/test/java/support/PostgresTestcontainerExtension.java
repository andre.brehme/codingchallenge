package support;

import org.junit.jupiter.api.extension.BeforeAllCallback;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.utility.DockerImageName;

import static org.junit.jupiter.api.extension.ExtensionContext.Namespace.GLOBAL;

public class PostgresTestcontainerExtension implements BeforeAllCallback, ExtensionContext.Store.CloseableResource {

    private static final PostgreSQLContainer postgresContainer = new PostgreSQLContainer(
            DockerImageName.parse("postgres:14.1").asCompatibleSubstituteFor("postgres"))
            .withDatabaseName("test")
            .withUsername("postgres")
            .withPassword("postgres");

    @Override
    public void beforeAll(ExtensionContext context) {
        if (!postgresContainer.isCreated()) {
            postgresContainer.start();

            System.out.println("PostgresUrl: " + postgresContainer.getJdbcUrl());
            System.setProperty("spring.datasource.url", postgresContainer.getJdbcUrl());
            System.setProperty("spring.datasource.username", postgresContainer.getUsername());
            System.setProperty("spring.datasource.password", postgresContainer.getPassword());

            context.getRoot().getStore(GLOBAL).put("PostgresTc", this);
        }
    }

    @Override
    public void close() {
        postgresContainer.stop();
    }

}


